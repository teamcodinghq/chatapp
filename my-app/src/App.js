import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h2>Welcome to Flippin Awesome Stuff</h2>
        </div>
        <p className="App-intro">
          stuff and words.
        </p>
        <Dice></Dice><Dice></Dice><Dice></Dice><Dice></Dice>
      </div>
    );
  }
}

function getDiceLetter() {
  return (Math.random().toString(36).substring(2,3));
}

class Dice extends Component {
  constructor(props) {
    super(props);
    this.state = {letter: getDiceLetter}
  }
  render(){
    return (
      <div className="Dice">
        {this.state.letter()}
      </div>
    )
  }
}

export default App;
