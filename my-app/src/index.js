import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import Dice from './App';
import registerServiceWorker from './registerServiceWorker';
import './index.css';

// ReactDOM.render(<App />, document.getElementById('root'));

ReactDOM.render(<Dice />, document.getElementById('root'));
registerServiceWorker();
